## Contributos

send an email to rgroux+formulaire@sauron-mordor.net and give me your gitlab username.


## Maths formula @ UPMC

|Module|Started|reviewed|Validated|
|:---:|:---:|:---:|:---:|
|2M216|Done|Not started|No|
|2M220|Done|Not started|No|
|2M261|Not started|Not started|No|
|2M270|In pogress|Not started|No|
|2M271|Not started|Not started|No|
|2M360|Not started|Not started|No|
                                                                                      
                                                                                                 
## Requirement:                                                                                  
                                                                                                                             
|Windows|Mac OSX|Linux|                                                                                                      
|:---:|:---:|:---:|                                                                                                          
|MikTex|MacTex|Xetex|                                                                                                        
|http://miktex.org|http://www.tug.org/mactex|sudo apt install texlive-xetex|                                                 
                                                                                                                                         
## How to use git                                                                                                                        
  - Intro to git                                                                                                                         
    > http://rogerdudler.github.io/git-guide/index.fr.html                                                                                           
                                                                                                                                                     
    > https://www.miximum.fr/blog/enfin-comprendre-git/                                                                                              
                                                                                                                                                     
  - Learn git in 15 minutes                                                                                                                          
    > https://try.github.io/levels/1/challenges/1                                                                                                              
                                                                                                                                                               
    > http://www.robusta.io/content/tutoriel/git/start-git.html                                                                                                
                                                                                                                                                               
    > https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git                                                                                       
                                                                                                                                                               
  - Windows/MacOSX                                                                                                                                             
    > https://www.sourcetreeapp.com/                                                                                                                           
                                                                                                                                                               
  - Install git on Linux                                                                                                                                       
    > apt install git                                                                                                                                          
                                                                                                                                                               
  - Create gitlab account                                                                                                                                      
    > https://gitlab.com/                                                                                                                                      
                                                                                                                                                               
  - Fork main project                                                                                                                                          
    because pusblishing in the master branch is never allowed, we must use working copy and after to merge request                                             
                                                                                                                                                                  
    > https://gitlab.com/Maths_at_UPMC/Formulaire/forks/new                                                                                           
                                                                                                                                                                  
  - Clone the forked repository (over ssh or https)                                                                                                               
    > git clone git@gitlab.com:<username>/Formulaire.git                                                                                                          
                                                                                                                                                                  
    or                                                                                                                                                            
                                                                                                                                                                  
    > git clone https://gitlab.com/<username>/Formulaire.git                                                                                                      
  - Setup git                                                                                                                                                     
                                                                                                                                                                  
    ```bash                                                                                                                                                       
    cd Formulaire                                                                                                                                                 
    git config user.name "User Name"
    git config user.email "Email@email"
    git config format.signoff true

    ```
    add upstream as remote branch
    
    ```bash
    git remote add upstream https://gitlab.com/Maths_at_UPMC/Formulaire.git
    
    ```
    or over ssh 
    ```bash
    git remote add upstream git@github.com:Maths_at_UPMC/Formulaire.git
    
    ```
    
  - Advanced users only - add gnupg signing keys if you care
  
    > https://git-scm.com/book/uz/v2/Git-Tools-Signing-Your-Work
    
  - git pull/push
    edit modify or add something into the repo
    
    ```bash
    git add <FILES>
    git commit 
    git push
    ```
  - Merge request
    Create a merge request and next
    
      > https://gitlab.com/<username>/Formulaire/merge_requests
    
    And click on new merge request
    
  - Update upstream
    
      ```bash
      git fetch origin master
      ```
  - Merge from upstream (update your repo from master)
  
    ```bash
    git merge upstream/master
    
    ```
